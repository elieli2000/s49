// alert("Hello World!")


// fetch() method in JavaScript is used to send request in the server and load the received response in the webpages. The request and response is in JSON format.

/*
	Syntax:
		fetch('url', options)
			- "url" this is the url which the request is to be made (endpoint)
			- "options" array of properties that contains the HTTP method, body of request, headers.
	
*/

// Get posts data
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((data) => showPosts(data));


// Add post data
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
		// Prevents the page from loading
		e.preventDefault()

		fetch("https://jsonplaceholder.typicode.com/posts", {
			method: "POST",
			body: JSON.stringify({
				title: document.querySelector("#txt-title").value,
				body: document.querySelector("#txt-body").value,
				userId: 1
			}),
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then((response) => response.json())
		.then((data)=> {
			console.log(data);
			alert("Successfully added!")
		})

		// resets the state of our input into blanks after submitting a new post.
		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
});


// View Posts
const showPosts = (posts) =>{
	//Create a variable that will contain all the posts
	let postEntries = "";

	// We will used forEach() to display each post from our jsonplaceholder API.
	posts.forEach((post) =>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	//To check what is stored in the postEntries variables
	// console.log(postEntries);

	//To replace the content of the "div-post-entries"
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}


const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title, and body from the post to be updated in the Edit Post Form
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	// removeAttribute() removes the disabled attribute/property.
	document.querySelector("#btn-submit-update").removeAttribute("disabled")
};


// Update post data
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

		e.preventDefault();

		let id = document.querySelector("#txt-edit-id").value;

		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: "PUT",
			body: JSON.stringify({
				id: id,
				title: document.querySelector("#txt-edit-title").value,
				body: document.querySelector("#txt-edit-body").value,
				userId: 1
			}),
			headers: {
				"Content-Type": "application/json"
			}

		})
		.then((response) => response.json())
		.then((data) => {
			console.log(data);
			alert("Successfully updated!")
		})

		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		document.querySelector("#btn-submit-update").setAttribute("disabled", true)
});


const deletePost = (id) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method:"DELETE",
        headers: {
            "Content-Type": "application/json"
        }
    })
   
    console.log('Post Deleted');
    
   	const element = document.getElementById(`post-${id}`);
   	element.remove();
}
